#include "http_handshake.h"

namespace ws {
	namespace http_handshake {
		std::string get_sid() {
			static const char *POLLING_URL_0 = "https://****";
			static const char *POLLING_URL_1 = "https://****";
			static const char *POLLING_URL_2 = "https://****";
			nlohmann::json json_ret;
			std::string sid;

			nsfq_http::http_connector::connector c;

			/* Start handshake, first result is discarded. */
			c.get(POLLING_URL_0);
			c.get(POLLING_URL_1);

			/* Get session id for websocket. */
			json_ret = nlohmann::json::parse(c.get_buffer().substr(1));
			sid = json_ret["sid"];

			/* Post "40" acknowledgement with sid. */
			c.post(POLLING_URL_2 + sid, "40");

			return sid;
		}
	}

}
